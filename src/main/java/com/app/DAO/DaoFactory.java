package com.app.DAO;

public class DaoFactory {

    public static Dao getDao(DaoType type) {
        switch (type) {
            case JDBC:
                return new DAOImpl();
            case JPA:
                return new DaoJPAImpl();
            default:
                throw new RuntimeException("type is not supported");
        }
    }



}
