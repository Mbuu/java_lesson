package com.app.DAO;

import com.app.models.Phone;

import java.util.List;

public interface Dao {
    public void write(Phone phone);

    public Phone read(Phone phone);

    public void update(int id, Phone phone);

    public void deleteById(int id);

    public void deleteByModel(String model);

    public List<Phone> readAll();

    public int count(int price);

    public double average(String model);

}
