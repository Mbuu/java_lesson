package com.app.DAO;

import com.app.models.Phone;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOImpl implements Dao {
    private DriverManager dm;

    public DAOImpl() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Connection getConnection() {
        try {
            return DriverManager.getConnection("jdbc:mysql://localhost:3306/phone_schema", "root", "8888");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void write(Phone phone) {
        try (Connection c = getConnection()) {
            PreparedStatement pc = c.prepareStatement("insert into phone (price, model) values (?, ?)");
            pc.setInt(1, phone.getPrice());
            pc.setString(2, phone.getModel());
            pc.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public Phone read(Phone phone) {
        try (Connection c = getConnection()) {
            PreparedStatement ps = c.prepareStatement("SELECT * from phone where (price = ? and model = ?)");
            ps.setInt(1, phone.getPrice());
            ps.setString(2, phone.getModel());
            ResultSet rs = ps.executeQuery();

            Phone phone1 = new Phone();

            while (rs.next()) {
                phone1.setModel(rs.getString("model"));
                phone1.setPrice(rs.getInt("price"));
            }

            return phone1;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(int id, Phone phone) {
        try (Connection c = getConnection()) {
            PreparedStatement ps = c.prepareStatement("UPDATE phone set price = ? and model = ? where id = ?");
            ps.setInt(1, phone.getPrice());
            ps.setString(2, phone.getModel());
            ps.setInt(3, id);
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void deleteById(int id) {
        try (Connection c = getConnection()) {
            PreparedStatement ps = c.prepareStatement("DELETE phone set id = ?");
            ps.setInt(1, id);
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteByModel(String model) {
        try (Connection c = getConnection()) {
            PreparedStatement ps = c.prepareStatement("DELETE phone set model = ?");
            ps.setString(1, model);
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Phone> readAll() {
        try (Connection c = getConnection()) {
            PreparedStatement ps = c.prepareStatement("SELECT * from phone");
            ResultSet rs = ps.executeQuery();

            List<Phone> resultList = new ArrayList<>();

            while (rs.next()) {
                Phone phone1 = new Phone();

                phone1.setModel(rs.getString("model"));
                phone1.setPrice(rs.getInt("price"));
                resultList.add(phone1);
            }

            return resultList;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int count(int price) {
        try (Connection c = getConnection()) {
            PreparedStatement ps = c.prepareStatement("SELECT count(*) from phone");
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getInt(1);
        }catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public double average(String model) {
        try (Connection c = getConnection()) {
            PreparedStatement ps = c.prepareStatement("SELECT avg(*) from phone");
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getInt(1);
        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
