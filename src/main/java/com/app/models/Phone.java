package com.app.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NamedQuery(name = "phone.avg", query = "select avg(p.price) from Phone p")
public class Phone {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    private int price;
    private String model;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return price == phone.price &&
                Objects.equals(model, phone.model);
    }

    @Override
    public int hashCode() {

        return Objects.hash(price, model);
    }
    /*@Override
    public int hashCode() {
        return price + (model == null ? 0 : model.length());
    }

    public boolean equals(Object o){
        if (o == null){
            return false;
        }
        if (o == this){
            return true;
        }
        if(o.getClass() == this.getClass()){
            Phone p = (Phone) o;
            if(p.price != price){
                return false;
            }
            if(p.model != null){
                return p.model.equals(model);
            }
            if(model == null){
                return true;
            }
        }
        return false;

    }*/

}
